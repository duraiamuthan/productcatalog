$(document).ready(function () {
    if ($(".table.data-table").length > 0) {
        $(".table.data-table").DataTable({
            "language": {
                "emptyTable": "No results found",
            }
        });
    }
});

function deleteProduct(id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }, function (result) {
        if (result) {
            window.location = '/product/' + id + '/delete';
        }
    })

}

function changeCommentStatus(e, commentId, status) {
    var that = e;
    $.ajax({
        url: '/product-comment/' + commentId + '/change-status',//  Routing.generate('comment_update', {'id': $(this).data('comment-id')}),
        data: {"commentStatus": status},
        dataType: "json",
        type: "post",
        success: function (data) {
            if (data.hasError == false) {

                $(that).parent().find(".active").addClass("btn-secondary").removeClass('active');
                $(that).parent().find(".status-a").removeClass('btn-info');
                $(that).parent().find(".status-r").removeClass('btn-danger');
                $(that).parent().find(".status-d").removeClass('btn-dark');
                if (status != "Pending") {
                    $(that).parent().find(".status-p").remove();
                }
                if (status == "Approved") {
                    $(that).parent().find(".status-a").addClass('btn-info active');
                    $(that).parent().find(".status-p").remove();
                    $(that).parent().find(".status-r").remove();
                }
                if (status == "Rejected") {
                    $(that).parent().find(".status-r").addClass('btn-danger active');
                    $(that).parent().find(".status-p").remove();
                    $(that).parent().find(".status-a").remove();
                }
                if (status == "Deleted") {
                    $(that).parent().find(".status-d").addClass('btn-dark active');
                    $(that).parent().find(".status-p").remove();
                    $(that).parent().find(".status-a").remove();
                    $(that).parent().find(".status-r").remove();
                }
            }
        }
    });
}