<?php

namespace App\Tests;

use App\Entity\Product;
use App\Entity\User;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use App\Security\Token\TestBrowserToken;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

class ProductControllerTest extends WebTestCase
{
    public function testListProductWithoutLogin()
    {
        $client = static::createClient();
        $client->request('GET', '/product');

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testProductList()
    {
        $client = $this->loginUser();

        // user is now logged in, so you can test protected resources
        $client->request('GET', '/product/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('.card-header', 'Product List');

    }

    public function loginUser(/*$user, string $firewallContext = 'main', $client*/)
    {
        $firewallContext = 'main';
        $client = static::createClient();
        $userRepository = $client->getContainer()->get('doctrine')->getRepository(User::class);
        $user = $userRepository->findOneByEmail('admin@admin.com');

        if (!interface_exists(UserInterface::class)) {
            throw new \LogicException(sprintf('"%s" requires symfony/security-core to be installed.', __METHOD__));
        }

        if (!$user instanceof UserInterface) {
            throw new \LogicException(sprintf('The first argument of "%s" must be instance of "%s", "%s" provided.', __METHOD__, UserInterface::class, \is_object($user) ? \get_class($user) : \gettype($user)));

        }

        $token = new TestBrowserToken($user->getRoles(), $user);
        $token->setAuthenticated(true);
        $session = $client->getContainer()->get('session');
        $session->set('_security_' . $firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);

        return $client;
    }

    public function testNewProduct()
    {
            $productName = 'testProduct ' . mt_rand();
            $productDescription = $this->generateRandomString(255);

            $client = $this->loginUser();
            $client->request('GET', '/product/create');

            $client->submitForm('Save', [
                'product[name]' => $productName,
                'product[description]' => $productDescription,
                'product[enabled]' => true,
            ]);

            $this->assertResponseRedirects('/product/', Response::HTTP_FOUND);

            /** @var \App\Entity\Product $product */
            $product = self::$container->get(ProductRepository::class)->findOneByName($productName);
            $this->assertNotNull($product);
            $this->assertSame($productDescription, $product->getDescription());

    }

    public function testEditProduct()
    {
        $client = $this->loginUser();

        $product = $client->getContainer()->get('doctrine')->getRepository(Product::class)->findOneBy([]);
        $newProductName = $product->getName() . "11";
        $productId = $product->getId();
        $client->request('GET', '/product/' . $productId . '/edit');
        $client->submitForm('Update', [
            'product[name]' => $newProductName,
            'product[description]' => $product->getDescription(),
        ]);

        $product = $client->getContainer()->get('doctrine')->getRepository(Product::class)->find($productId);
        $this->assertSame($newProductName, $product->getName());
    }

    private function generateRandomString(int $length): string
    {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return mb_substr(str_shuffle(str_repeat($chars, ceil($length / mb_strlen($chars)))), 1, $length);
    }

}