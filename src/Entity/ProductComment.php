<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductCommentRepository")
 */
class ProductComment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="productComments")
     */
    private $productId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="productComments")
     */
    private $authorUserId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="productComments")
     */
    private $approvedUserId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $comment;

    /**
     * @ORM\Column(type="string",length=20,columnDefinition="ENUM('Pending', 'Approved', 'Rejected', 'Deleted')")
     */
    private $commentStatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCommentStatus(): ?string
    {
        return $this->commentStatus;
    }

    public function setCommentStatus(string $commentStatus): self
    {
        $this->commentStatus = $commentStatus;

        return $this;
    }

    public function getProductId(): ?Product
    {
        return $this->productId;
    }

    public function setProductId(?Product $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    public function getAuthorUserId(): ?User
    {
        return $this->authorUserId;
    }

    public function setAuthorUserId(?User $authorUserId): self
    {
        $this->authorUserId = $authorUserId;

        return $this;
    }

    public function getApprovedUserId(): ?User
    {
        return $this->approvedUserId;
    }

    public function setApprovedUserId(?User $approvedUserId): self
    {
        $this->approvedUserId = $approvedUserId;

        return $this;
    }
}
