<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductComment;
use App\Form\ProductCommentType;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="product", methods={"GET"})
     */
    public function index(Request $request, ProductRepository $productRepository, PaginatorInterface $paginator)
    {
        $isAdminUser = ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'));
        $q = $request->query->get('q');
        $products = $productRepository->getQueryBuilder($q, $isAdminUser)->getQuery()->getResult();

        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'products' => $products,
        ]);
    }

    /**
     * @Route("/{id}/comments", name="product_pending_comment", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function pendingComment(Request $request, Product $product)
    {
        $isAdminUser = ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'));
        $comments = $product->getPendingComments();
        return $this->render('product/pending_comment.html.twig', [
            'controller_name' => 'ProductController',
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/create", name="product_add", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $product->setCreatedAt(new \DateTime());
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product');
        }

        return $this->render('product/add.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function edit(Request $request, Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product');
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="product_detail", methods={"GET"})
     */
    public function detail(Product $product): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ProductCommentRepository = $entityManager->getRepository(ProductComment::class);
        $user = $this->getUser();
        $comments = $ProductCommentRepository->getActiveComments($product, $user);
        return $this->render('product/detail.html.twig', [
            'product' => $product,
            'comments' => $comments
        ]);
    }

    /**
     * @Route("/{id}/delete", name="product_delete", methods={"DELETE", "GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function delete(Request $request, Product $product): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($product);
        $entityManager->flush();

        return $this->redirectToRoute('product');
    }
}
