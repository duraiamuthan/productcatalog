<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
/**
 * Class ErrorController
 * @package App\Controller
 * @Route("/error")
 */
class ErrorController extends AbstractController
{
//    protected $debug; // this is passed as a parameter from services.yaml
//    protected $code;  // 404, 500, etc.
//    protected $data;
//
//    public function __construct(BaseController $base, bool $debug) {
//
//        $this->debug = $debug;
//
//        $this->data = $base->data;
//
//    }

    /**
     * @Route("/403", name="unauthorized", methods={"GET"})
     */
    public function index()
    {
        return $this->render('error/index.html.twig', [
            'controller_name' => 'ErrorController',
        ]);
    }

    public function show(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        // dd($exception); // uncomment me to see the exception

        $template = 'error/error' . $exception->getStatusCode() . '.html.twig';
        return new Response($this->renderView($template, []));
    }
}
