<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductComment;
use App\Form\ProductCommentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ProdutCommentController extends AbstractController
{
    /**
     * @Route("/produt/comment", name="produt_comment")
     */
    public function index()
    {
        return $this->render('produt_comment/index.html.twig', [
            'controller_name' => 'ProdutCommentController',
        ]);
    }

    /**
     * @Route("/product/{id}/comment/new", name="comment_new", methods={"POST"})
     */
    public function newComment(Request $request, Product $product): Response
    {
        $ProductComment = new ProductComment();
        $user = $this->getUser();
        $comment = $request->request->get('comment');
        $ProductComment->setComment($comment);
        $ProductComment->setCreatedAt(new \DateTime());
        $ProductComment->setUpdatedAt(new \DateTime());
        $ProductComment->setCommentStatus("Pending");
        $ProductComment->setProductId($product);
        $ProductComment->setAuthorUserId($user);

        $em = $this->getDoctrine()->getManager();

        $product->addProductComment($ProductComment);

        $em->persist($ProductComment);
        $em->flush();

        return $this->redirectToRoute('product_detail', ['id' => $product->getId()]);
    }

    /**
     * @Route("/product-comment/{id}/change-status", name="comment_update", methods={"POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function updateComment(Request $request, ProductComment $ProductComment): Response
    {
        $commentStatus = $request->request->get('commentStatus');
        $ProductComment->setUpdatedAt(new \DateTime());
        $ProductComment->setCommentStatus($commentStatus);
        $em = $this->getDoctrine()->getManager();
        $em->persist($ProductComment);
        $em->flush();

        return new JsonResponse(
            ['hasError'=>false]
        );
    }
}
