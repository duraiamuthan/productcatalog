<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductComment;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductComment[]    findAll()
 * @method ProductComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductComment::class);
    }

    public function getActiveComments(Product $product, User $user)
    {
        $qb = $this->createQueryBuilder('p');
        return $qb
            //->andWhere('p.commentStatus = :status')
            ->where($qb->expr()->orX(
                $qb->expr()->eq('p.commentStatus', ':status'),
                $qb->expr()->eq('p.authorUserId', ':user')
            ))
            ->andWhere('p.productId = :val')
            ->setParameter('val', $product)
            ->setParameter('status', 'Approved')
            ->setParameter('user', $user)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
    // /**
    //  * @return ProductComment[] Returns an array of ProductComment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductComment
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
